# Introduction to PDCurses #

This repository contains sample CodeLite workspace with some example C programs illustrating how to use PDCurses library.

## Repository contents and structure ##

* `PDCursesIntroduction.workspace` - The main workspace file for CodeLite IDE. Open it with CodeLite and it will load all projects.
* `*.project` - Project files for each program contained in the workspace. CodeLite will open them all automatically when it loads the workspace.

## Programs and their purpose ##

1. `SmokeTest` - the first 'Hello, World!' program for checking if PDCurses are working. It should display colour banner, wait a few seconds and exit. Besides checking basic curses capabilities it also illustrates:
    * Initializing PDCurses engine
    * Hiding the text cursor
    * Creating and deleting windows
    * Printing characters and updating windows
    * Waiting for a time interval
    * Exiting from PDCurses

2. `Caleidoscope` - random visual pattern generator using colour routines. The concepts it illustrates include:
    * Initializing and using colours and character attributes
    * Resizing the terminal window
    * Reading keyboard without waiting for user
    * Writing a character at arbitrary location on the screen

3. `LightsOut` - simple prototype of Lights Out game, without game logic. It demonstrates following concepts:
    * Creating and painting multiple windows
    * Reading cursor keys from the keyboard

4. `Paint` - colour painting with mouse in ASCII semi-graphic. Concepts demonstrated in this project:
    * Enabling mouse in PDCurses
    * Responding to mouse button events
    * Determining the window and coordinates of click event
    * Responding to mouse wheel events